﻿$(function () {

  var images = [
    'Screenshot from 2019-07-01 13-31-26.png',
    'D-JWhr3U8AA4JGD.jpg:large.jpeg',
    'D-I6eYBXUAAEant.jpg:large.jpeg',
    'Exxon-NYSE.jpg',
    'D9wj4voXoAA3oih.jpg',
    'D9sUJkLXoAAI9q2.jpg:large.jpeg',
    'D9zUA6EXUAAss_m.jpg',
    '083-_FM_4197_cropped-1800*1800-550ee9.JPG',
    '0.png',
    '1519010140582-we-talked-to-the-guy-who-photographs-plane-crashes-141-1415756828.jpeg',
    '1519010165580-we-talked-to-the-guy-who-photographs-plane-crashes-126-1415756828.jpeg',
    '1519010192322-we-talked-to-the-guy-who-photographs-plane-crashes-733-1415756829.jpeg',
    '1519661781688-10_Juan-Arredondo_2.jpeg',
    '1519667354664-09_Philippe_Duduoit.jpeg',
    '2B2Qwrq.gif',
    '3295.jpg',
    '50a08d8fe5eaf9f628217c6ca854861dba5684b1.jpeg',
    '56e05e399105841c008b5380-960-674.jpg',
    'bda72224-7e2e-4c71-8389-415397c3d472.jpeg',
    'chimney chutney.jpg',
    'Coal-Photo-1.png',
    'D1adIJGXQAAaUb8.jpg:large.jpeg',
    'D1DvqhDWwAEJNsS.jpg:large.jpeg',
    'D1DvxoPWwAMYBBA.jpg:large.jpeg',
    'D3WFCtYXoAAdQDy.jpg:large.jpeg',
    'D4apqpCXoAAbyGF.jpg:large.jpeg',
    'D4Jc9NPWkAApPEH.jpg:large.jpeg',
    'D5jB7i6XsAk4NE0.jpg:large.jpeg',
    'D6_-PL1WsAEGJlR.jpg',
    'D6_-PLGWwAAaAPY.jpg',
    'D6_-PMEXoAANRZ0.jpg',
    'D6xVN1xW4AAUNgz.jpg:large.jpeg',
    'D7JymYeXkAUrmCe.jpg:large.jpeg',
    'D7PZpT5XoAENFqM.jpg:large.jpeg',
    'D7SPIi1XoAE-yqR.jpg:large.jpeg',
    'D8EiDA1WwAMhn7F.jpg:large.jpeg',
    'D8EtOmhXsAARP-G.jpg:large.jpeg',
    'D8EX2g9XoAASEPS.jpg:large.jpeg',
    'D8UlwZ3UYAIPupj.jpg:large.jpeg',
    'D90K_grW4AEBH1e.jpg:large.jpeg',
    'D9e5DY7WsAEfwv7.jpg:large.jpeg',
    'D9MP06bXYAADlSY.jpg:large.jpeg',
    'DiJjSOUXUAAK42n.jpg:large.jpeg',
    'Ds4inkfWoAEacwU.jpg:large.jpeg',
    'Ds5XUEhWoAAmgzF.jpg:large.jpeg',
    'Dwaij6UWkAEHFwH.jpg',
    'DzSRGv_WkAAwobO.jpg',
    'ec9683cb-0e27-4e2a-870c-f0f7136e1ea6.jpeg',
    'explore-through-the-lens-alex-honnold.adapt.1900.1.jpg',
    'Fieldbrook-Stump-300dpi_web.jpg',
    'file-20190507-103060-47t1ag.jpg',
    'file-20190507-103085-roidcb.jpg',
    'uploads.jpg',
    'white people explaining black people how to work.jpeg'
  ];

  var randomNumber = Math.floor(Math.random() * images.length);
  var setRandomImage = function () {
    document.getElementsByTagName('body')[0].style['height'] = document.documentElement.clientHeight + "px";
    document.getElementsByTagName('body')[0].style['background-image'] = "url(img/" + images[randomNumber] + ")";
    randomNumber = (randomNumber + 1) % (images.length);
  };

  setInterval(setRandomImage, 3000);

  setRandomImage();

  var noop = new Array()
	function preload() {
		for (i = 0; i < preload.arguments.length; i++) {
			noop[i] = new Image()
			noop[i].src = "img/" + preload.arguments[i];
		}
	}
	preload.apply(null, images);

  var device;

  log('Requesting Token...');
  $.getJSON('https://jet-bongo-2166.twil.io/capability-token')
    .then(function (data) {
      log('Got a token.');
      console.log('Token: ' + data.token);

      // Setup Twilio.Device
      device = new Twilio.Device(data.token, {
        // Set Opus as our preferred codec. Opus generally performs better, requiring less bandwidth and
        // providing better audio quality in restrained network conditions. Opus will be default in 2.0.
        codecPreferences: ['opus', 'pcmu'],
        // Use fake DTMF tones client-side. Real tones are still sent to the other end of the call,
        // but the client-side DTMF tones are fake. This prevents the local mic capturing the DTMF tone
        // a second time and sending the tone twice. This will be default in 2.0.
        fakeLocalDTMF: true,
      });

      device.on('ready',function (device) {
        log('Device Ready!');
        call();
      });

      device.on('error', function (error) {
        log('Device Error: ' + error.message);
      });

      device.on('connect', function (conn) {
        log('Ignore the "free account, bla bla voice". Waiting for Ernst to pick up his phone!');
        document.getElementById('call-controls').style.display = 'inline';
        document.getElementById('button-call').style.display = 'none';
        document.getElementById('button-hangup').style.display = 'inline';
        bindVolumeIndicators(conn);
      });

      device.on('disconnect', function (conn) {
        log('Call ended.');
        document.getElementById('button-call').style.display = 'inline';
        document.getElementById('button-hangup').style.display = 'none';
      });

      device.on('incoming', function (conn) {
        log('Incoming connection from ' + conn.parameters.From);
        var archEnemyPhoneNumber = '+12093373517';

        if (conn.parameters.From === archEnemyPhoneNumber) {
          conn.reject();
          log('It\'s your nemesis. Rejected call.');
        } else {
          // accept the incoming connection and start two-way audio
          conn.accept();
        }
      });

    })
    .catch(function (err) {
      console.log(err);
      log('Could not get a token from server!');
    });

  // Make call
  var call = function () {
    // get the phone number to connect the call to
    var params = {
      To: '+0000000000'
    };

    console.log('Calling ' + params.To + '...');
    if (device) {
      device.connect(params);
    }
  };

  // Bind button to make call
  document.getElementById('button-call').onclick = call;

  // Bind button to hangup call
  document.getElementById('button-hangup').onclick = function () {
    log('Hanging up...');
    if (device) {
      device.disconnectAll();
    }
  };

  function bindVolumeIndicators(connection) {
    var inputVol = 0;
    var outputVol = 0;
    setTimeout(function () {
      if (inputVol < 0.1) {
        log('Ernst can probably not hear you, check mic');
      } else {
        log('mic volume seems good');
      }
      if (outputVol < 0.1) {
        log('You can probably not hear Ernst, but he might hear you...');
      } else {
        log('output volume seems good');
      }

    }, 5000);
    connection.on('volume', function(inputVolume, outputVolume) {
      inputVol = Math.max(inputVol, inputVolume);
      outputVol = Math.max(outputVol, outputVolume);
    });
  }

  // Activity log
  function log(message) {
    var logDiv = document.getElementById('log');
    logDiv.innerHTML += '<p><mark>&gt;&nbsp;' + message + '</mark></p>';
    logDiv.scrollTop = logDiv.scrollHeight;
  }

});
